from django.db import models

from django.contrib.auth.models import AbstractUser

class User(AbstractUser):
    is_mancraft = models.BooleanField(default=False)
    is_customer = models.BooleanField(default=False)

class Mancraft(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    # Add additional fields for the mancraft role

class Customer(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    # Add additional fields for the customer role
