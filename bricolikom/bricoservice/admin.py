from django.contrib import admin
from django.contrib import admin
from .models import User, Mancraft, Customer

admin.site.register(User)
admin.site.register(Mancraft)
admin.site.register(Customer)
