from django.apps import AppConfig


class BricoserviceConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'bricoservice'
