from django.shortcuts import render

# Create your views here.
from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate, logout
from .forms import UserRegisterForm
from .models import Mancraft, Customer
from django.contrib.auth.decorators import login_required

from django.contrib import messages
from django.urls import reverse
from .forms import UserLoginForm  


@login_required
def home_view(request):
    context = {}
    if request.user.is_authenticated:
        # Check if the user is a mancraft
        if request.user.is_mancraft:
            mancraft = Mancraft.objects.get(user=request.user)
            context['role'] = 'Mancraft'
            context['mancraft_data'] = mancraft
        # Check if the user is a customer
        elif request.user.is_customer:
            customer = Customer.objects.get(user=request.user)
            context['role'] = 'Customer'
            context['customer_data'] = customer

    return render(request, 'bricoservice/home.html', context)



def register(request):
    if request.method == 'POST':
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            user = form.save()
            if user.is_mancraft:
                Mancraft.objects.create(user=user)
            else:
                Customer.objects.create(user=user)
            login(request, user)
            return redirect('home')
    else:
        form = UserRegisterForm()
    return render(request, 'bricoservice/register.html', {'form': form})

def login_view(request):
    # If the request is a POST, try to pull out the relevant info.
    if request.method == 'POST':
        form = UserLoginForm(request, data=request.POST)
        if form.is_valid():
            # Use form.cleaned_data to retrieve username and password.
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            
            # Use Django's authenticate method to try and verify the credentials.
            user = authenticate(username=username, password=password)
            if user is not None:
                # If the credentials are valid, log the user in.
                login(request, user)
                # Redirect the user to the home page or another appropriate page.
                return redirect('home')  # Make sure you have a URL pattern named 'home'
            else:
                # If credentials are not valid, display an error message.
                messages.error(request, 'Invalid username or password.')
    else:
        # If the request is not a POST, create a blank form instance.
        form = UserLoginForm()

    # Render the login template with the form.
    return render(request, 'bricoservice/login.html', {'form': form})


@login_required
def logout_view(request):
    logout(request)
    messages.info(request, 'You have successfully logged out.')
    return redirect('login')  # Redirect to login page